import * as React from 'react';
import ReactRenderer from "../src/React/ReactRenderer";
import * as reactDom from 'react-dom';
import {mocked} from "ts-jest/utils";
import ReactMiddleware from "../src/React/ReactMiddleware";

jest.mock('react-dom');

describe('ReactRenderer', () => {
    it('can render default element via reactDom.render', () => {
        const renderer = new ReactRenderer();

        const testElement = (<div>test</div>);

        renderer.defaultElement = testElement;

        renderer.render();

        expect(mocked(reactDom.render)).toBeCalledWith(testElement, null);
    });

    it('passes the default element to middlewares', () => {
        class TestMiddleware implements ReactMiddleware {
            extend(element: JSX.Element): JSX.Element {
                return element;
            }
        }

        const middleware = new TestMiddleware();
        middleware.extend = jest.fn();

        const renderer = new ReactRenderer();
        renderer.registerMiddleware(middleware);

        const testElement = (<div>test</div>);
        renderer.defaultElement = testElement;
        renderer.render();

        expect(middleware.extend).toBeCalledWith(testElement);
    });

    it('renders elements returned by middleware', () => {
        const testElement = (<div>test</div>);

        class TestMiddleware implements ReactMiddleware {
            extend(element: JSX.Element): JSX.Element {
                return testElement;
            }
        }

        const renderer = new ReactRenderer();
        renderer.registerMiddleware(new TestMiddleware());

        renderer.render();

        expect(mocked(reactDom.render)).toBeCalledWith(testElement, null);
    });

    it('chains middlewares, passing one return value to the nexts extend', () => {
        const testElement = (<div>test</div>);

        class TestMiddleware implements ReactMiddleware {
            extend(element: JSX.Element): JSX.Element {
                return testElement;
            }
        }

        const renderer = new ReactRenderer();
        renderer.registerMiddleware(new TestMiddleware());

        const receivingMiddleware = new TestMiddleware();
        receivingMiddleware.extend = jest.fn();
        renderer.registerMiddleware(receivingMiddleware);

        renderer.render();

        expect(receivingMiddleware.extend).toBeCalledWith(testElement);
    });

    it('chains middlewares based on priority', () => {
        const testElement = (<div>test</div>);

        class TestMiddleware implements ReactMiddleware {
            extend(element: JSX.Element): JSX.Element {
                return testElement;
            }
        }

        const renderer = new ReactRenderer();

        const receivingMiddleware = new TestMiddleware();
        receivingMiddleware.extend = jest.fn();
        renderer.registerMiddleware(receivingMiddleware, 1);

        renderer.registerMiddleware(new TestMiddleware(), 10);

        renderer.render();

        expect(receivingMiddleware.extend).toBeCalledWith(testElement);
    });
})
