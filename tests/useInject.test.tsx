import {bootApplication, prepareDocument} from "./lib";
import GloominateProvider, {ReactRenderer} from "../";
import useInject from "../src/Injection/useInject";
import * as React from "react";
import InjectionProvider from "../src/Injection/InjectionProvider";

describe('useInject', () => {
    beforeEach(() => {
        prepareDocument();
    });

    it('should return a dependency from the container', async () => {
        function TestComponent() {
            const test = useInject<string>('test');

            return <div>{test}</div>;
        }

        const app = bootApplication([
            GloominateProvider,
            InjectionProvider
        ]);

        app.container.add({
            test: 'expected value'
        });
        app.container.get<ReactRenderer>('Renderer').defaultElement = (<TestComponent />);

        app.render();

        expect(document.body.textContent).toMatch('expected value');
    });
});