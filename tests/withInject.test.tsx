import * as React from 'react';
import GloominateProvider, {withInject, ReactMiddleware, RouteStore, ReactRenderer} from "../";
import {bootApplication, prepareDocument} from "./lib";

describe('withInjection', () => {
    beforeEach(() => {
        prepareDocument();
    });

    it('can inject dependencies', () => {

        interface Props {
            value: string,
            injected: string
        }

        interface InjectedProps {
            injected: string
        }

        class TestElement extends React.Component<Props>{
            render() {
                const {value, injected} = this.props;
                return value+' - '+injected;
            }
        }

        const InjectedTestElement = withInject<Props, InjectedProps>(TestElement, {
            injected: 'test'
        });

        const app = bootApplication([
            GloominateProvider
        ]);

        app.container.add({'test': 'Kekse'});
        app.container.get<ReactRenderer>('Renderer').defaultElement = (<div key='test'><InjectedTestElement value='oink' /></div>);

        app.render();

        expect(document.body.textContent).toMatch('oink - Kekse');
    });
});