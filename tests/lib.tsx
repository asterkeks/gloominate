import * as React from 'react';
import Application, {Provider} from 'providify';
import InjectionContext from "../src/Injection/InjectionContext";
import {render} from "@testing-library/react";
import {Container} from 'providify';

export function prepareDocument() {
    const container = document.createElement(("div"));
    container.id = 'app';
    document.body.appendChild(container);
}

export function bootApplication(providers: typeof Provider[]) {
    const app = new Application();

    app.setProviders(providers);

    app.boot();

    return app;
}

interface ContainerCallback {
    (container: Container): void
}

export function renderWithContainer(element: JSX.Element, hydrateContainer: ContainerCallback = () => {}) {
    const container = new Container;

    hydrateContainer(container);

    const reactReturn = render(
        <InjectionContext.Provider value={container}>
            {element}
        </InjectionContext.Provider>
    );

    return {
        ...reactReturn,
    };
}
