import * as React from 'react';
import {Renderer} from "providify";
import ReactProvider from "../src/React/ReactProvider";
import ReactRenderer from "../src/React/ReactRenderer";
import {bootApplication} from "./lib";

describe('ReactProvider', () => {
    test('ReactProvider sets renderer to ReactRenderer', async() => {
        const app = bootApplication([
            ReactProvider
        ]);

        const renderer = app.container.get<Renderer>('Renderer');

        expect(renderer).toBeInstanceOf(ReactRenderer);
    });
});
