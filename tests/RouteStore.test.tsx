import * as React from 'react';
import {RouteStore} from "..";
import {Route} from 'react-router-dom'
import Adapter from 'enzyme-adapter-react-16';
import { configure } from 'enzyme';
configure({ adapter: new Adapter() });


describe('RouteStore', () => {
    let routeStore: RouteStore;
    beforeEach(() => {
        routeStore = new RouteStore();
    });

    it('can receive unspecified routes', () => {
        const route = <Route/>;
        routeStore.addRoute(route);

        const routes = routeStore.getRoutes();
        expect(routes.length).toBe(1);
        expect(routes[0]).toBe(route);
    });

    it('can receive specific routes', () => {
        const route = <Route/>;
        routeStore.addRoute(route, 'navigation');

        const routes = routeStore.getRoutes('navigation');
        expect(routes.length).toBe(1);
        expect(routes[0]).toBe(route);
    });

    it('can discern specific and unspecific routes', () => {
        const route = <Route path='/'/>;
        routeStore.addRoute(route);

        const specificRoute = <Route path='/specific' />;
        routeStore.addRoute(specificRoute, 'navigation');

        const unspecificRoutes = routeStore.getRoutes();
        expect(unspecificRoutes.length).toBe(1);
        expect(unspecificRoutes[0]).toMatchObject(route);

        const specificRoutes = routeStore.getRoutes('navigation');
        expect(specificRoutes.length).toBe(1);
        expect(specificRoutes[0]).toMatchObject(specificRoute);

        expect(specificRoutes[0]).not.toMatchObject(unspecificRoutes[0]);
    });

    it('returns an empty set for an unknown route bucket', () => {
        const routes = routeStore.getRoutes('does not exist');

        expect(Array.isArray(routes)).toBeTruthy();
        expect(routes.length).toBe(0);
    });
});