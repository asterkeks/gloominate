import Application, {Provider} from 'providify';
import ReactProvider from "./src/React/ReactProvider";
import InjectionProvider from "./src/Injection/InjectionProvider";

export {default as InjectionContext} from './src/Injection/InjectionContext';
export {default as ReactMiddleware} from './src/React/ReactMiddleware';
export {default as ReactRenderer} from './src/React/ReactRenderer';
export {default as RouteStore} from './src/React/RouteStore';
export {default as withInject} from './src/Injection/withInject';
export {default as useInject} from './src/Injection/useInject';
export {bootApplication, renderWithContainer} from './tests/lib';

export default class GloominateProvider extends Provider {
    register() {
        this.container.get<Application>('Application').addProvider(InjectionProvider);
        this.container.get<Application>('Application').addProvider(ReactProvider);
    }
}