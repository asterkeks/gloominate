import {Provider} from "providify";
import InjectionMiddleware from "./InjectionMiddleware";
import ReactRenderer from "../React/ReactRenderer";

export default class InjectionProvider extends Provider {
    boot() {
        const middleware = new InjectionMiddleware(this.container);
        this.container.get<ReactRenderer>('Renderer').registerMiddleware(middleware, 0);
    }
}