import * as React from "react";
import DIContainer from "rsdi";

const ContainerContext = React.createContext<DIContainer|null>(null);
ContainerContext.displayName = 'Container';

export default ContainerContext;
