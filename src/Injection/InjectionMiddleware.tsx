import * as React from 'react';
import DIContainer from "rsdi";
import {ReactMiddleware} from "../../index";
import ContainerContext from "./InjectionContext";

export default class InjectionMiddleware implements ReactMiddleware {
    container: DIContainer;

    constructor(container: DIContainer) {
        this.container = container;
    }

    extend(element: JSX.Element): JSX.Element {
        return (
            <ContainerContext.Provider value={this.container}>
                {element}
            </ContainerContext.Provider>
        );
    }

}