import {useContext} from "react";
import InjectionContext from "./InjectionContext";

export default function useInject<T>(dependency: string): T {
    const container = useContext(InjectionContext);

    if(!container) {
        throw new Error('Container not present');
    }

    return container.get<T>(dependency);
}