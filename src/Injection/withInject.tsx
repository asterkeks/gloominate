import * as React from 'react';
import {IDIContainer} from 'rsdi';
import InjectionContext from "./InjectionContext";

// thanks to: https://radzserg.medium.com/https-medium-com-radzserg-dependency-injection-in-react-part-3-f4fd3620a9b0
interface DIResolutionConfig {
  [propName: string]: string;
}

type Without<CombinedType, TypeToBeExcluded> = {
  [L in Exclude<keyof CombinedType, TypeToBeExcluded>]: CombinedType[L]
};

export default function withInject<AllProps, OnlyInjectedProps>(
  WrappedComponent: React.ComponentType<AllProps>,
  diConfig: DIResolutionConfig
): React.ComponentType<Without<AllProps, keyof OnlyInjectedProps>> {
  const name = WrappedComponent.displayName || WrappedComponent.name;

  class InjectionWrapper extends React.Component<any, any> {
    static contextType = InjectionContext;
    static displayName = `withInject(${name})`;
    static WrappedComponent = WrappedComponent;

    render() {
      const diContainer = this.context as IDIContainer;
      const diProps: any = {};
      Object.keys(diConfig).forEach((propName: string) => {
        const definitionName = diConfig[propName];
        diProps[propName] = diContainer.get(definitionName);
      });

      const { ...restProps } = this.props;
      return <WrappedComponent {...diProps} {...(restProps as AllProps)} />;
    }
  }

  return InjectionWrapper;
}