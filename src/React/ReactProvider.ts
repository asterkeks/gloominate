import {Provider} from "providify";
import ReactRenderer, {routeStore} from "./ReactRenderer";
import {factory} from "rsdi";
import BrowserRouterMiddleware from "./BrowserRouterMiddleware";

export default class ReactProvider extends Provider {
    register() {
        this.container?.add({
            'RouteStore': routeStore,
            'Renderer': factory(() => {
                const renderer = new ReactRenderer();
                renderer.registerMiddleware(new BrowserRouterMiddleware(), 100);
                return renderer;
            })
        })
    }
}
