import * as React from "react";
import {
    Switch,
} from "react-router-dom";
import withInject from "../Injection/withInject";
import RouteStore from "./RouteStore";

interface Props {
    which: string,
    routeStore: RouteStore
}

interface InjectedProps {
    routeStore: RouteStore
}

class Router extends React.Component<Props> {
    render() {
        return (
            <Switch>
                {this.props.routeStore.getRoutes(this.props.which)}
            </Switch>
        );
    }
};

const InjectedRouter = withInject<Props, InjectedProps>(Router, {
    'routeStore': 'RouteStore'
});
export default InjectedRouter;