import * as React from "react";
import {Renderer} from "providify";
import * as reactDom from 'react-dom';
import Router from "./Router";
import ReactMiddleware from "./ReactMiddleware";
import RouteStore from "./RouteStore";

export const routeStore = new RouteStore();

export default class ReactRenderer implements Renderer {
    middlewares: ReactMiddleware[][] = [];
    defaultElement = (<Router which='main'/>);

    render() {
        let element = this.defaultElement;

        const result  = this.letMiddlewaresExtendElement(element);

        reactDom.render(result, document.getElementById('app'));
    }

    letMiddlewaresExtendElement(element: JSX.Element) {
        return this.middlewares.reverse().reduce<JSX.Element>((element: JSX.Element, middleswares: ReactMiddleware[]) => {
            return middleswares.reduce<JSX.Element>((element: JSX.Element, middleware: ReactMiddleware) => {
                return middleware.extend(element);
            }, element);
        }, element);
    }

    registerMiddleware(middleware: ReactMiddleware, priority: number = 1000) {
        if(!this.middlewares[priority]) {
            this.middlewares[priority] = [];
        }

        this.middlewares[priority].push(middleware);
    }

}
