import * as React from "react";
import {ReactMiddleware} from "../../index";
import {BrowserRouter} from "react-router-dom";

export default class BrowserRouterMiddleware implements ReactMiddleware {
    extend(element: JSX.Element): JSX.Element {
        return (<BrowserRouter>{element}</BrowserRouter>);
    }
}