type RoutesByBucket = {
    [name: string]: JSX.Element[]
};

export default class RouteStore {
    routes: RoutesByBucket = {};

    addRoute(route: JSX.Element, bucket: string = 'main') {
        if (this.bucketMissing(bucket)) {
            this.routes[bucket] = [];
        }

        this.routes[bucket].push(route);
    }

    getRoutes(bucket: string = 'main') {
        if(this.bucketMissing(bucket)) {
            return [];
        }

        return this.routes[bucket];
    }

    bucketMissing(bucket: string) {
        return !this.routes[bucket];
    }
}
