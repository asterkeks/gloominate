import * as React from 'react';
import withInject from "../../Injection/withInject";
import User from "./User";
import {Redirect} from 'react-router-dom'
import Routes from "./Routes";

interface Props {
    user: User,
    routes: Routes,
}

interface InjectedProps {
    user: User,
    routes: Routes,
}

class OnlyGuest extends React.Component<Props> {
    render() {
        if(this.props.user) {
            return (
                <Redirect to={this.props.routes.homeRoute()} />
            );
        }
    }
}

export default withInject<Props, InjectedProps>(OnlyGuest, {
    'user': 'currentUser',
    'routes': 'Routes'
});
