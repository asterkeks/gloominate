import * as React from 'react';
import withInject from "../../Injection/withInject";
import User from "./User";
import Routes from "./Routes";

interface Props {
    user: User,
    routes: Routes,
    redirect?: string,
    children: () => JSX.Element
}

interface InjectedProps {
    user: User,
    routes: Routes,
}

class RequiresUser extends React.Component<Props> {
    render() {
        if(!this.props.user) {
            if(this.props.redirect) {
                return (
                    <Redirect to={this.props.routes.homeRoute()} />
                );
            }

            return null;
        }

        return this.props.children();
    }
}

export default withInject<Props, InjectedProps>(RequiresUser, {
    'user': 'currentUser'
});
