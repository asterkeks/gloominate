export default interface ReactMiddleware {
    extend(element: JSX.Element): JSX.Element;
}
